﻿

using Android.App;
using Android.OS;
using Android.Widget;
using SQLite.Net.Platform.XamarinAndroid;
using AgilisaApp.Core;

namespace AgilisaApp.Droid
{
    [Activity(Label = "HomeActivity")]            
    public class HomeActivity : Activity
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public int Id { get; set; }

        private IUsuarioRepositorio _usuarioRepositorio;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.Home);

            var userInfoTextView = FindViewById<TextView>(Resource.Id.userInfoTextView);

            AsignData();

            userInfoTextView.Text = string.Format("Welcome back {0} {1}!!, \n Username: {2}", Name, LastName, UserName);
        }

        private void AsignData()
        {

            var platform = new SQLitePlatformAndroid();
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            IDatabaseHelper dbHelper = TinyIoC.TinyIoCContainer.Current.Resolve<DatabaseHelper>();

            Id = Intent.GetIntExtra(ContextParameter.ID, -1);

            _usuarioRepositorio = new UsuarioRepositorio(dbHelper);

            var usrLocal = _usuarioRepositorio.GetById(Id);

            if (usrLocal != null)
            {
                Name = usrLocal.Nombre;
                LastName = usrLocal.Apellido;
                UserName = usrLocal.Username;

            }


            var users = _usuarioRepositorio.GetAll();

        }
    }
}


﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace AgilisaApp.Core
{

    public interface IAGOnlineClient : IDisposable
    {
        Task<HttpResponseMessage> GetAsync(string resource);
    }

    public class AGOnlineClient : IAGOnlineClient
    {
        private readonly HttpClient _httpClient;
        private readonly string ApiUrl = "http://10.211.55.3:6415/api/";

        public AGOnlineClient()
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri(ApiUrl);
        }

        public AGOnlineClient(string apiUrl)
        {
            if (!string.IsNullOrEmpty(apiUrl))
            {
                ApiUrl = apiUrl;
            }
        }

        public async Task<HttpResponseMessage> GetAsync(string resource)
        {
            var response = await _httpClient.GetAsync(resource);

            return response;
        }

        public void Dispose()
        {
            _httpClient.Dispose();
        }
    }

    public static class AGOnlineClientFactory
    {
        private static IAGOnlineClient _client;

        public static IAGOnlineClient Create()
        {
            if (_client == null)
            {
                _client = new AGOnlineClient();
            }

            return _client;
        }
    }
}

